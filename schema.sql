CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
);


CREATE TABLE IF NOT EXISTS polls (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  slug TEXT NOT NULL UNIQUE,
  authorId INTEGER NOT NULL,
  title TEXT NOT NULL,
  description TEXT,
  maxChoices INTEGER DEFAULT 1,
  views INTEGER DEFAULT 0,
  
  FOREIGN KEY(authorId) REFERENCES users(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS choices (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  pollId INTEGER NOT NULL,
  title TEXT NOT NULL,
  description TEXT,

  FOREIGN KEY(pollId) REFERENCES polls(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS votes (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  choiceId INTEGER NOT NULL,
  userId INTEGER,

  UNIQUE(choiceId, userId),
  FOREIGN KEY(choiceId) REFERENCES choices(id) ON DELETE CASCADE,
  FOREIGN KEY(userId) REFERENCES users(id) ON DELETE CASCADE
);

-- Initial data
BEGIN TRANSACTION;
  DELETE FROM choices WHERE pollId = 1;
  INSERT INTO polls (id, authorId, slug, title) VALUES (1, 0, 'test', 'Testing poll?');
  INSERT INTO choices (pollId, title) VALUES 
    (1, "Yes"),
    (1, "No"),
    (1, "Maybe");
END TRANSACTION;
