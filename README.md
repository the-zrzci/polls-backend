# Polls backend

## Requirements

- [Bun](https://bun.sh/)

## Installation

1. `bun install` - install dependencies

Run with `bun run --hot .`

## API Endpoints

Any endpoint may return a non-200 status code and the body will contain `{ error: string }`.\
If the body is just a string without object, then that error was not supposed to happen and the code needs changes.

---

### POST `/api/polls`

Creates a new poll.
_Requires session cookie!_

**Body:** `PollCreate` from APITypes.ts

**Returns:** Id of the new poll. (`{ id: number }`)

---

### GET `/api/polls/{poll_slug}`

**Returns:** `Poll` from APITypes.ts

---

### DELETE `/api/polls/{poll_slug}`

Delete the poll.

**Returns:** `{ ok: true }`

---

### PUT `/api/polls/{poll_slug}`

Update the poll.

**Returns:** `{ id: number }`

---

### POST `/api/polls/{poll_slug}/vote`

Votes on a poll with given id.
_Session cookie optional, returns poll cookie_

**Body:** The database id of the choice to vote for (`{ choiceId: number }`)

**Returns:** `{ ok: true }`

---

### GET `/api/polls/{poll_slug}/results`

**Returns:** `PollResults` from APITypes.ts

---

### EventSource `/api/polls/{poll_slug}/sse`

Sends an event named `voteAdded` with `{ choiceId: number }` when a vote is added to poll with given slug.

---

### GET `/api/user`

Get info about the currently logged in user and their polls or 403 if not logged in.
_Requires session cookie!_

**Returns:** `UserWithPolls` from APITypes.ts.

---

### POST `/api/user/login`

Logs in a user.

**Body:** `{ username: string, password: string }`

**Returns:** `User` from APITypes.ts

---

### POST `/api/user/register`

Registers and logs in a user.

**Body:** `{ username: string, password: string }`

**Returns:** `User` from APITypes.ts

---

### GET `/api/user/logout`

Logs out the current user user. _Requires session cookie!_

**Returns:** `{ ok: true }`
