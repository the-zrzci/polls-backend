type SSEEmit<E extends string = string, T = any> = (event: E, data: T) => Promise<void>;

/** Replacement of an EventEmitter */
export class SSEEmitter<Events> {
  readonly #domains = new Map<string, SSEEmit[]>();

  async emit<E extends keyof Events>(domain: string, event: E, data: Events[E]): Promise<void> {
    const emits = this.#domains.get(domain);
    if (!emits) return;

    const promises = emits.map((emit) => emit(event as string, data));

    await Promise.all(promises);
  }

  addEmit(domain: string, emit: SSEEmit) {
    let emits = this.#domains.get(domain);
    if (!emits) {
      emits = [];
      this.#domains.set(domain, emits);
    }
    emits.push(emit);
  }

  removeEmit(domain: string, emit: SSEEmit) {
    const emits = this.#domains.get(domain);
    if (!emits) return;

    const index = emits.indexOf(emit);
    if (index === -1) return;

    emits.splice(index, 1);
  }
}

export function createSSE<T>(
  request: Request,
  emitter: SSEEmitter<T>,
  domain: string,
  init: ResponseInit = {}
): Response {
  return new Response(
    new ReadableStream({
      type: "direct",
      async pull(controller) {
        const emit = async (event: string, data: unknown) => {
          await controller.write(`event: ${event}\ndata: ${JSON.stringify(data)}\n\n`);
          await controller.flush();
        };

        const close = () => {
          emitter.removeEmit(domain, emit);
          controller.close();
        };

        emitter.addEmit(domain, emit);

        request.signal.addEventListener("abort", close);
        request.signal.addEventListener("close", close);

        await emit("hello", undefined);

        return new Promise(() => void 0);
      },
    }),
    {
      status: 200,
      ...init,
      headers: {
        "Content-Type": "text/event-stream",
        "Cache-Control": "no-cache, no-store",
        "X-Accel-Buffering": "no",
        Connection: "keep-alive",
        Pragma: "no-cache",
        Expires: "0",
        ...init.headers,
      },
    }
  );
}
