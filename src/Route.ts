import type { Server } from "bun";
import type { URL } from "url";

/** Object that is passed to routes */
export interface RouteContext {
  path: string;
  url: URL;
  request: Request;
  server: Server;
  cookies: Record<string, string>;
}

/** Function that handles a request or doesn't. */
export type Route = (context: RouteContext) => Response | void | Promise<Response | void>;

/** A response that is JSON-encoded. */
export class JSONResponse extends Response {
  constructor(body: unknown, statusOrInit: number | ResponseInit = 200) {
    super(JSON.stringify(body), typeof statusOrInit === "number" ? { status: statusOrInit } : statusOrInit);
    this.headers.set("Content-Type", "application/json");
  }
}

/** Creates and returns a JSONResponse with error format */
export const httpError = (message = "Internal server error :(", status = 500, moreData: Record<string, unknown> = {}) =>
  new JSONResponse({ error: message, ...moreData }, status);

/** Throws HTTP error if the request method doesn't match */
export function checkMethod(request: Request, method: string) {
  if (request.method != method) throw new JSONResponse("Method Not Allowed", 405);
}

type Format = Record<string, "string" | "number" | "array">;
type FormatToType<T extends Format> = {
  [Key in keyof T]: T[Key] extends "string" ? string : T[Key] extends "array" ? unknown[] : number;
};

/* Make sure the given body is in given format */
export function validateBody<T extends Format>(body: unknown, format: T) {
  // Check body type
  if (!body || typeof body !== "object") throw httpError("Body must be object", 422);

  // Check body content
  for (const key in format) {
    if (!(key in body)) throw httpError(`Missing key '${key}' in body`, 422);
    const _key = key as keyof typeof body;

    // check array
    if (format[key] === "array") {
      if (!Array.isArray(body[_key])) throw httpError(`Key '${key}' in body must be an array`, 422);
    }

    // check type
    else if (typeof (body[_key] as unknown) !== format[key]) {
      throw httpError(`Key '${key}' in body must be a ${format[key]}`, 422);
    }
  }

  return body as FormatToType<T>;
}

/** Tries to get and parse body from request, check it for format and return it */
export async function getBody<T extends Format>(request: Request, format: T) {
  if (!request.body) throw httpError("Expected body", 400);

  // Download body
  let body: unknown;
  try {
    body = await request.json();
  } catch (err) {
    throw httpError("Body is not valid JSON", 400);
  }

  return validateBody<T>(body, format);
}
