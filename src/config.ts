export const config = {
  databasePath: process.env.DATABASE_PATH || "data/database.sqlite",
  authDuration: 24 * 60 * 60 * 1000, // 24 hours
  authCookie: "polls-auth",
  pollCookieDuration: 10 * 365 * 24 * 60 * 60 * 1000, // 10 years?
  pollCookie: "polls-votes",
  compressCookies: true,
  cookieSecret: process.env.COOKIE_SECRET || "megatajneheslo",
};
