import { Database } from "bun:sqlite";
import Path from "node:path";
import { mkdir } from "node:fs/promises";
import type { User, Choice, Poll, PollCreate, UserWithPassword, PollResults } from "../APITypes";
import { config } from "../config";

await mkdir(Path.dirname(config.databasePath), { recursive: true });
const db = new Database(config.databasePath, { create: true });
// Speed improvement thingy
db.exec("PRAGMA journal_mode = WAL;");

// Create db tables if they don't exist
const schema = Bun.file("schema.sql");
db.run(await schema.text());

type Id = { id: number };

// Prepared statements
const getUserByName = db.prepare<UserWithPassword, { $name: string }>(`
  SELECT * FROM users WHERE name = $name
`);
const insertUser = db.prepare<User, { $name: string; $password: string }>(`
  INSERT INTO users (name, password) VALUES ($name, $password) RETURNING id, name
`);
const deleteUser = db.prepare<Id, { $userId: number }>(`
  DELETE FROM users WHERE id = $userId RETURNING ID
`);
const insertPoll = db.prepare<
  Poll,
  {
    $authorId: number;
    $slug: string;
    $title: string;
    $description?: string;
    $maxChoices?: number;
  }
>(`
  INSERT INTO polls (authorId, slug, title, description, maxChoices) VALUES ($authorId, $slug, $title, $description, $maxChoices) RETURNING id
`);
const deletePoll = db.prepare<Id, { $pollSlug: string; $userId: number }>(`
  DELETE FROM polls WHERE slug = $pollSlug AND authorId = $userId RETURNING id
`);
const updatePoll = db.prepare<
  Poll,
  { $pollSlug: string; $userId: number; $newSlug: string; $title: string; $description?: string; $maxChoices: number }
>(`
  UPDATE polls SET slug = $newSlug, title = $title, description = $description, maxChoices = $maxChoices WHERE slug = $pollSlug AND authorId = $userId RETURNING id
`);
const insertChoice = db.prepare<Id, { $pollId: number; $title: string; $description?: string }>(`
  INSERT INTO choices (pollId, title, description) VALUES ($pollId, $title, $description) RETURNING id
`);
const updateChoice = db.prepare<Id, { $choiceId: number; $title: string; $description?: string }>(`
  UPDATE choices SET title = $title, description = $description WHERE id = $choiceId RETURNING id
`);
const deleteNotChoices = db.prepare<null, { $pollId: number; $keepChoicesIds: string }>(`
  DELETE FROM choices WHERE pollId = $pollId AND id NOT IN (SELECT value FROM json_each($choiceIds))
`);
const getPollBySlug = db.prepare<Poll, { $slug: string }>(`
  SELECT * FROM polls WHERE slug = $slug
`);
const getUserPolls = db.prepare<Poll, { $userId: number }>(`
  SELECT * FROM polls WHERE authorId = $userId
`);
const addPollView = db.prepare<Id, { $pollId: number }>(`
  UPDATE polls SET views = views + 1 WHERE id = $pollId RETURNING id
`);
const getPollChoices = db.prepare<Choice, { $pollId: number }>(`
  SELECT * FROM choices WHERE pollId = $pollId
`);
const getPollChoice = db.prepare<Choice, { $pollId: number; $choiceId: number }>(`
  SELECT choices.* FROM choices 
    WHERE choices.pollId = $pollId AND choices.id = $choiceId
`);
const getPollVotes = db.prepare<{ choiceId: number; count: number }, { $pollId: number }>(`
  SELECT choiceId, COUNT(*) as count FROM votes
    JOIN choices ON votes.choiceId = choices.id
    WHERE choices.pollId = $pollId
    GROUP BY choiceId
`);
const getUserPollVotes = db.prepare<{ count: number }, { $pollId: number; $userId: number }>(`
  SELECT COUNT(*) as count FROM votes
    JOIN choices ON votes.choiceId = choices.id
    WHERE choices.pollId = $pollId AND votes.userId = $userId
`);
const addVote = db.prepare<Id, { $choiceId: number; $userId?: number }>(`
  INSERT INTO votes (choiceId, userId) VALUES ($choiceId, $userId) RETURNING id
`);

const BEGIN = () => db.exec("BEGIN");
const COMMIT = () => db.exec("COMMIT");
const ROLLBACK = () => db.exec("ROLLBACK");

// Exported interface
export const database = {
  /** Close the database */
  close() {
    db.close();
  },

  /** Returns a user (with password hash!) or null if not found */
  getUser(username: string): UserWithPassword | null {
    return getUserByName.get({ $name: username });
  },

  /** Creates a user and return */
  createUser(username: string, password: string): User | null {
    return insertUser.get({ $name: username, $password: password });
  },

  /** Get all polls for a user */
  getUserPolls(userId: number): Poll[] {
    return getUserPolls.all({ $userId: userId });
  },

  /** Delete a user */
  deleteUser(userId: number) {
    return deleteUser.get({ $userId: userId });
  },

  /** Get a poll with it's choices or null if not found */
  getPoll(slug: string): Poll | null {
    const poll = getPollBySlug.get({ $slug: slug });
    if (!poll) return null;
    poll.choices = getPollChoices.all({ $pollId: poll.id });
    return poll;
  },

  /** Add a view to a poll */
  addPollView(id: number): boolean {
    return !!addPollView.get({ $pollId: id });
  },

  /** Creates a poll and return */
  createPoll(authorId: number, pollData: PollCreate) {
    try {
      BEGIN();
      const poll = insertPoll.get({
        $authorId: authorId,
        $slug: pollData.slug,
        $title: pollData.title,
        $description: pollData.description,
        $maxChoices: pollData.maxChoices,
      });

      if (!poll) throw new Error("Failed to insert poll???");

      const choices = [];
      for (const choice of pollData.choices) {
        if (!choice.title) continue;
        const id = insertChoice.run({ $pollId: poll.id, $title: choice.title, $description: choice.description });
        choices.push(id);
      }

      if (choices.length == 0) throw new Error("Cannot have poll with zero choices");

      COMMIT();
      return poll.id;
    } catch (err) {
      ROLLBACK();
      throw err;
    }
  },

  /** Deletes poll */
  deletePoll(pollSlug: string, userId: number): boolean {
    return !!deletePoll.get({ $pollSlug: pollSlug, $userId: userId });
  },

  updatePoll(pollSlug: string, userId: number, pollData: PollCreate) {
    try {
      BEGIN();
      const poll = updatePoll.get({
        $pollSlug: pollSlug,
        $userId: userId,
        $newSlug: pollData.slug,
        $title: pollData.title,
        $description: pollData.description,
        $maxChoices: pollData.maxChoices,
      });

      if (!poll) throw new Error("Poll not found");

      //check if choice has id if so update else inserte
      const choices = [];
      for (const choice of pollData.choices) {
        if (!choice.title) continue;

        let id;
        if (choice.id) {
          id = updateChoice.get({ $choiceId: choice.id, $title: choice.title, $description: choice.description });
        } else {
          id = insertChoice.get({ $pollId: poll.id, $title: choice.title, $description: choice.description });
        }
        choices.push(id);
      }

      if (choices.length == 0) throw new Error("Cannot have poll with zero choices");

      deleteNotChoices.run({
        $pollId: poll.id,
        $keepChoicesIds: JSON.stringify(choices),
      });

      COMMIT();
      return poll.id;
    } catch (err) {
      ROLLBACK();
      throw err;
    }
  },

  /** Vote for something */
  addVotes(pollSlug: string, choices: number[], userId?: number) {
    try {
      BEGIN();

      const poll = getPollBySlug.get({ $slug: pollSlug });
      if (!poll) throw new Error("Given poll does not exist");

      if (userId) {
        // NOTE: Maybe this is app logic and should be elsewhere?
        const votes = getUserPollVotes.get({ $pollId: poll.id, $userId: userId });
        if (votes && votes.count > 0) throw new Error("Already voted!");
      }

      const choicesSet = new Set(choices);

      if (choicesSet.size > poll.maxChoices) {
        throw new Error("Too many choices");
      }

      for (const choiceId of choicesSet) {
        // Verify that poll ID and choice ID matches
        const choice = getPollChoice.get({ $pollId: poll.id, $choiceId: choiceId });
        if (!choice) throw new Error("Given poll does not have the given choice");

        const vote = addVote.get({
          $choiceId: choiceId,
          $userId: userId,
        });
        if (!vote) throw new Error("Vote failed??");
      }
      COMMIT();
    } catch (err) {
      ROLLBACK();
      throw err;
    }
  },

  /** Get poll results */
  getPollResults(pollSlug: string): PollResults | null {
    const poll = database.getPoll(pollSlug) as PollResults | null;
    if (!poll) return null; // Not found

    const votes = getPollVotes.all({ $pollId: poll.id });
    // Try to assign each choice it's number of votes
    for (const choice of poll.choices) {
      const voteI = votes.findIndex((v) => v.choiceId == choice.id);
      if (voteI == -1) {
        // Not found
        choice.votes = 0;
        continue;
      }
      const [vote] = votes.splice(voteI, 1);
      choice.votes = vote.count;
    }

    // Database should've deleted them
    if (votes.length) console.warn("Found votes for not existing choices:", votes);

    return poll;
  },
};
