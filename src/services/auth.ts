import { APIError, type User } from "../APITypes";
import { JSONResponse } from "../Route";
import { config } from "../config";
import { createCookie, decodeCookie, encodeCookie } from "./cookies";
import { database } from "./database";

export const emptyUserCookie = createCookie(config.authCookie, "", 0);

/** Returns user in cookie, throws if missing */
export async function getCookieUser(cookies: Record<string, string>, optional?: false): Promise<User>;
export async function getCookieUser(cookies: Record<string, string>, optional: true): Promise<User | undefined>;
export async function getCookieUser(cookies: Record<string, string>, optional = false): Promise<User | undefined> {
  if (cookies[config.authCookie]) {
    const user = await decodeCookie<User>(cookies[config.authCookie]);
    if (user?.id) return user;
  }
  if (!optional) throw new JSONResponse({ error: APIError.NOT_LOGGED_IN }, 403);
}

/** Returns user as cookie */
export async function createUserCookie(user: User) {
  return createCookie(config.authCookie, await encodeCookie(user), config.authDuration);
}

/** Verifies given password for given username and returns that user (without password hash) or false if failed */
export async function verifyLogin(username: string, password: string): Promise<false | User> {
  const userWithPassword = database.getUser(username);
  if (!userWithPassword) return false;

  const { password: pwHash, ...user } = userWithPassword;

  if (await Bun.password.verify(password, pwHash)) {
    return user;
  }
  return false;
}
