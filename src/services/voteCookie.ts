import { config } from "../config";
import { createCookie, decodeCookie, encodeCookie } from "./cookies";

type VoteLog = string[];

/** Logs that user voted for a poll, returns undefined if already voted */
export async function logVoteToCookie(cookies: Record<string, string>, pollSlug: string) {
  const data: VoteLog = cookies[config.pollCookie] ? await decodeCookie(cookies[config.pollCookie], true) : [];

  if (data.includes(pollSlug)) return;
  data.push(pollSlug);

  return createCookie(config.pollCookie, await encodeCookie(data, true), config.pollCookieDuration);
}
