import Cookie from "cookie";
import { config } from "../config";

const KEY = await crypto.subtle.importKey(
  "raw",
  Buffer.from(config.cookieSecret),
  { name: "HMAC", hash: "SHA-512" },
  false,
  ["sign", "verify"]
);

/** Parses and returns cookies from header */
export function getCookies(request: Request): Record<string, string> {
  const cookies = request.headers.get("Cookie");
  if (!cookies) return {};
  return Cookie.parse(cookies);
}

/** Creates a cookie string */
export function createCookie(name: string, value: string, expiresIn?: number) {
  return Cookie.serialize(name, value, {
    expires: expiresIn ? new Date(Date.now() + expiresIn) : undefined,
    httpOnly: true,
    secure: process.env.NODE_ENV === "production",
    sameSite: "none",
    path: "/",
  });
}

/** Creates a JWT-ish token from data */
export async function encodeCookie(data: unknown, compress = false): Promise<string> {
  const dataBuffer = Buffer.from(JSON.stringify(data), "utf-8");
  const signature = await crypto.subtle.sign("HMAC", KEY, dataBuffer);

  if (compress) {
    const prefix = new Uint8Array([dataBuffer.length]);
    const data = Buffer.concat([prefix, dataBuffer, Buffer.from(signature)]);

    return Buffer.from(
      Bun.gzipSync(data, {
        level: 9,
        memLevel: 9,
        // @ts-expect-error windowBits should be -9 to -15 but that doesn't work, -2 does to remove headers
        windowBits: -2,
      })
    ).toString("base64url");
  }

  const signatureString = Buffer.from(signature).toString("base64url");
  const cookie = "." + dataBuffer.toString("base64url") + "." + signatureString;
  return cookie;
}

/** Verify our JWT-ish token and return it's data */
export async function decodeCookie<T>(cookie: string, compressed = false): Promise<T> {
  let dataBuffer: Buffer;
  let signature: Buffer;

  if (compressed) {
    const cookieData = Bun.gunzipSync(Buffer.from(cookie, "base64url"));
    const prefix = cookieData[0];
    dataBuffer = Buffer.from(cookieData.slice(1, prefix + 1));
    signature = Buffer.from(cookieData.slice(prefix + 1));
  } else {
    const parts = cookie.split(".");
    if (parts.length != 3) throw new InvalidCookieError();
    dataBuffer = Buffer.from(parts[1], "base64url");
    signature = Buffer.from(parts[2], "base64url");
  }

  const isValid = await crypto.subtle.verify("HMAC", KEY, signature, dataBuffer);
  if (!isValid) throw new InvalidCookieError();
  return JSON.parse(dataBuffer.toString("utf-8"));
}

export class InvalidCookieError extends Error {
  status = 400;

  constructor() {
    super("Invalid cookie");
  }
}
