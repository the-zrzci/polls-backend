import { JSONResponse, checkMethod, type Route, getBody, httpError } from "../Route";
import { database } from "../services/database";
import { APIError, type User } from "../APITypes";
import { SQLiteError } from "bun:sqlite";
import { createUserCookie, emptyUserCookie, getCookieUser, verifyLogin } from "../services/auth";

export const userModule: Route = async ({ path, request, cookies }) => {
  switch (path) {
    case "/": {
      if (request.method === "GET") {
        const user = await getCookieUser(cookies);
        // Check if user is in db
        const dbUser = database.getUser(user.name);
        if (!dbUser) throw new JSONResponse({ error: APIError.NOT_LOGGED_IN }, 403);

        const polls = database.getUserPolls(user.id);
        return new JSONResponse({ ...user, polls });
      }

      // Delete user
      checkMethod(request, "DELETE");
      const user = await getCookieUser(cookies);
      database.deleteUser(user.id);
      return new JSONResponse(
        { ok: true },
        {
          headers: { "Set-Cookie": emptyUserCookie },
        }
      );
    }

    case "/login": {
      checkMethod(request, "POST");
      const { username, password } = await getBody(request, {
        username: "string",
        password: "string",
      });
      const user = await verifyLogin(username, password);

      if (user) return loginResponse(user);
      throw httpError(APIError.WRONG_PASSWORD, 403);
    }

    case "/register": {
      checkMethod(request, "POST");
      const { username, password } = await getBody(request, {
        username: "string",
        password: "string",
      });
      const pwHash = await Bun.password.hash(password);

      try {
        const user = database.createUser(username, pwHash);
        if (!user) throw httpError("Database didn't return user id after insert?");

        return loginResponse(user);
      } catch (err) {
        // Catch user exists error
        if (err instanceof SQLiteError && err.code == "SQLITE_CONSTRAINT_UNIQUE") {
          throw httpError(APIError.USER_EXISTS, 409);
        }
        // Everything else throw again
        throw err;
      }
    }

    case "/logout": {
      await getCookieUser(cookies);
      return new JSONResponse(
        { ok: true },
        {
          headers: { "Set-Cookie": emptyUserCookie },
        }
      );
    }
  }
};

async function loginResponse(user: User): Promise<Response> {
  return new JSONResponse(user, {
    headers: { "Set-Cookie": await createUserCookie(user) },
  });
}
