import { SQLiteError } from "bun:sqlite";
import { APIError, SLUG_RE, type PollCreate, type PollSSEEvents } from "../APITypes";
import { JSONResponse, checkMethod, type Route, getBody, httpError, validateBody } from "../Route";
import { getCookieUser } from "../services/auth";
import { logVoteToCookie } from "../services/voteCookie";
import { database } from "../services/database";
import { SSEEmitter, createSSE } from "../utils/sse";

/** Handles communication within the app */
const sse = new SSEEmitter<PollSSEEvents>();

export const pollsModule: Route = async (context) => {
  const { path, request, cookies } = context;

  // POST / => create poll
  if (path == "/") return createPoll(context);

  // /<id>...
  const pathSegments = path.slice(1).split("/");
  const pollSlug = pathSegments.shift();
  const subPath = "/" + pathSegments.join("/");
  if (!pollSlug) return; // Missing poll slug

  switch (subPath) {
    case "/": {
      // GET a poll
      if (request.method == "GET") {
        const poll = database.getPoll(pollSlug);
        if (poll) {
          database.addPollView(poll.id);
          await sse.emit(pollSlug, "viewed", void 0);
          return new JSONResponse(poll);
        } else {
          throw httpError(APIError.NOT_FOUND, 404);
        }
      }
      // DELETE a poll
      else if (request.method == "DELETE") {
        const user = await getCookieUser(cookies);
        if (database.deletePoll(pollSlug, user.id)) {
          return new JSONResponse({ ok: true });
        } else {
          throw httpError(APIError.NOT_FOUND, 404);
        }
      }
      // PUT/update a poll
      else if (request.method == "PUT") {
        const pollData = createPollData(request);
        const user = await getCookieUser(cookies);

        try {
          const id = database.updatePoll(pollSlug, user.id, await pollData);
          return new JSONResponse({ id }, 201);
        } catch (err) {
          if (err instanceof Error) {
            if (err.message === "Poll not found") {
              throw httpError(APIError.NOT_FOUND, 404);
            } else if (err.message == "Cannot have poll with zero choices") {
              throw httpError(APIError.NO_CHOICES, 400);
            }
          }

          throw err;
        }
      }
      return;
    }

    // Vote
    case "/vote": {
      checkMethod(request, "POST");
      const user = await getCookieUser(cookies, true);
      const { choices } = (await getBody(request, { choices: "array" })) as { choices: number[] };

      // Check if user voted by cookie
      const voteCookie = await logVoteToCookie(cookies, pollSlug);
      if (!voteCookie) throw httpError(APIError.ALREADY_VOTED, 409);

      try {
        database.addVotes(pollSlug, choices, user?.id);
        // Voted
        await sse.emit(pollSlug, "voteAdded", choices);
        return new JSONResponse({ ok: true }, { status: 201, headers: { "Set-Cookie": voteCookie } });
      } catch (err) {
        // Catch database errors
        if (err instanceof Error) {
          switch (err.message) {
            case "Given poll does not exist":
              throw httpError(APIError.NOT_FOUND, 404);
            case "Already voted!":
              throw httpError(APIError.ALREADY_VOTED, 409);
            case "Given poll does not have the given choice":
              throw httpError(APIError.NOT_FOUND, 404);
            case "Too many choices":
              throw httpError(APIError.TOO_MANY_CHOICES, 400);
          }
        }
        // Catch this user already voted
        if (err instanceof SQLiteError && err.code == "SQLITE_CONSTRAINT_UNIQUE") {
          throw httpError(APIError.ALREADY_VOTED, 409);
        }
        throw err;
      }
    }

    case "/results": {
      checkMethod(request, "GET");
      const poll = database.getPollResults(pollSlug);
      if (poll) {
        return new JSONResponse(poll);
      } else {
        throw httpError(APIError.NOT_FOUND, 404);
      }
    }

    case "/sse": {
      if (request.method === "HEAD") {
        return new Response("", { status: 200 });
      }
      checkMethod(request, "GET");
      return createSSE(request, sse, pollSlug);
    }
  }
};

/** Create a poll route (it was too long so I put it in a function) */
const createPoll: Route = async ({ request, cookies }) => {
  checkMethod(request, "POST");
  const user = await getCookieUser(cookies);

  const pollData = createPollData(request);

  // Create poll
  try {
    const id = database.createPoll(user.id, await pollData);
    return new JSONResponse({ id }, 201);
  } catch (err) {
    if (err instanceof SQLiteError && err.code == "SQLITE_CONSTRAINT_UNIQUE") {
      throw httpError(APIError.POLL_EXISTS, 409);
    } else if (err instanceof Error && err.message == "Cannot have poll with zero choices") {
      throw httpError(APIError.NO_CHOICES, 400);
    }
    throw err;
  }
};

async function createPollData(request: Request): Promise<PollCreate> {
  const pollData = (await getBody(request, {
    slug: "string",
    title: "string",
    maxChoices: "number",
    choices: "array",
  })) as unknown as PollCreate;

  // Validate choices
  for (const choice of pollData.choices) {
    validateBody(choice, { title: "string" });
  }

  // Check slug
  if (!SLUG_RE.test(pollData.slug)) throw httpError(APIError.INVALID_SLUG, 400);

  return pollData;
}
