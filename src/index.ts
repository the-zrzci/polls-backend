import { httpError, type Route, type RouteContext } from "./Route";
import { pollsModule } from "./modules/polls";
import { userModule } from "./modules/user";
import { getCookies } from "./services/cookies";

/** Map of modules; prefix -> route function */
const MODULES: Record<string, Route> = {
  "/api/polls": pollsModule,
  "/api/user": userModule,
};

export const server = Bun.serve({
  async fetch(request, server) {
    // Parse URL
    const url = new URL(request.url);
    // Parse cookies
    const cookies = getCookies(request);

    // Try to find the module by URL
    for (const prefix in MODULES) {
      if (url.pathname.startsWith(prefix)) {
        // Get path without prefix and remove trailing slash or just slash
        const path = url.pathname.slice(prefix.length).replace(/\/$/, "") || "/";
        // Try to run the module
        const response = await runRoute(MODULES[prefix], { path, url, request, server, cookies });

        // If we got a response, return it
        if (response) return response;
        // Else send 404
        break;
      }
    }

    // Send 404 Not Found
    return httpError("Route not found", 404);
  },
});

/** Tries to run the route but handles errors */
async function runRoute(route: Route | undefined, context: RouteContext) {
  try {
    // Found a module, execute it
    return (await route?.(context)) ?? undefined;
  } catch (err) {
    if (err instanceof Response) {
      // If the error is a response, use it
      return err;
    } else if (err instanceof Error && "status" in err) {
      // If the error has a status, use it
      return httpError(err.message, Number(err.status));
    }
    // Otherwise log it and send generic response
    console.error(err);
    return httpError();
  }
}

// App started
console.log("Listening on http://" + server.hostname + ":" + server.port);
