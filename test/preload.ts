import os from "node:os";
import Path from "node:path";
import { config } from "../src/config";

export const TMP_DIR = Path.join(os.tmpdir(), "Polls-test-" + Math.random());

// Reconfigure the app before tests
config.databasePath = Path.join(TMP_DIR, "database.sqlite");
