import { describe, it, expect } from "bun:test";
import { fetchAPI } from "./utils";
import { PollCreate } from "../src/APITypes";

const getTestPoll = (): PollCreate => ({
  slug: "test_" + Math.random(),
  title: "test poll",
  maxChoices: 0,
  choices: [],
});

describe("polls module", () => {
  it("fails to get nonexistent poll", async () => {
    const { result, data } = await fetchAPI("/api/polls/69");
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("can get a poll", async () => {
    const { result, data } = await fetchAPI("/api/polls/test");
    expect(result.ok).toBe(true);
    expect(data).toMatchObject({ id: 1, slug: "test" });
  });

  it("fails to create poll without session", async () => {
    const poll = getTestPoll();
    const { result, data } = await fetchAPI("/api/polls", poll);
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });
});
