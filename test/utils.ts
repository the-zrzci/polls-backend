import { server } from "../src";

export async function fetchAPI<T = any>(
  path: string,
  postData?: unknown,
  cookies?: string[],
  jsonStringifyData = true
) {
  const cookieHeader = cookies ? ({ Cookie: cookies.join("; ") } as {}) : {};

  const request = new Request(
    new URL(path, `http://${server.hostname}`),
    postData
      ? {
          method: "POST",
          credentials: "include",
          body: jsonStringifyData ? JSON.stringify(postData) : (postData as string),
          headers: {
            "Content-Type": "application/json",
            ...cookieHeader,
          },
        }
      : { headers: cookieHeader }
  );
  const result = await server.fetch(request);

  return {
    result,
    data: (await result.json()) as T | { error: string },
    cookies: result.headers.getSetCookie(),
  };
}
