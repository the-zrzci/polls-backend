import { TMP_DIR } from "./preload"; // must be first!
import fs from "node:fs/promises";
import { afterAll } from "bun:test";
import { database } from "../src/services/database";

afterAll(async () => {
  database.close();
  // Delete the temporary folder
  fs.rm(TMP_DIR, { recursive: true, force: true });
});
