import { describe, it, expect } from "bun:test";
import { fetchAPI } from "./utils";
import { config } from "../src/config";

describe("user module", () => {
  it("fails to get user without cookie", async () => {
    const { result, data } = await fetchAPI("/api/user");
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("fails wrong login", async () => {
    const { result, data } = await fetchAPI("/api/user/login", { username: "a", password: "a" });
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("fails totally invalid login", async () => {
    const { result, data } = await fetchAPI("/api/user/login", "peepoo", undefined, false);
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("fails to logout without cookie", async () => {
    const { result, data } = await fetchAPI("/api/user/logout");
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("fails to get user with garbage cookie", async () => {
    const { result, data } = await fetchAPI("/api/user", undefined, [config.authCookie + "=aaaaaaaaaaaa"]);
    expect(result.ok).toBe(false);
    expect(data).toHaveProperty("error");
  });

  it("registers, logins and gets user", async () => {
    const creds = { username: "b", password: "b" };
    let userId = -1;
    let cookie: string[] = [];

    {
      const { result, data } = await fetchAPI("/api/user/register", creds);
      expect(result.ok).toBe(true);
      expect(data).toHaveProperty("id");
      userId = data.id;
    }

    {
      const { result, data, cookies } = await fetchAPI("/api/user/login", creds);
      expect(result.ok).toBe(true);
      expect(data).toMatchObject({ id: userId });
      expect(cookies).toBeArrayOfSize(1);
      cookie = cookies;
    }

    {
      const { result, data } = await fetchAPI("/api/user", undefined, cookie);
      expect(result.ok).toBe(true);
      expect(data).toMatchObject({ id: userId });
    }
  });
});
